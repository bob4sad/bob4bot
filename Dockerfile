FROM ubuntu:20.04
WORKDIR /bob4bot
COPY . .
RUN apt update && apt-get -y install curl
RUN apt -y install nodejs && apt -y install npm 
RUN npm cache clean -f && npm install -g n && n stable
RUN npm i && npm i -g nodemon